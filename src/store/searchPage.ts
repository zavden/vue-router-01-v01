import { defineStore } from "pinia";
import { ref } from "vue"

export const useSearchPage = defineStore('searchPage', () => {
  const searchPage = ref<number>(1)
  const actualPage = ref<string>("")
  if(localStorage.getItem("searchPage"))
    searchPage.value = +<string>localStorage.getItem("searchPage")
  if(localStorage.getItem("actualPage"))
    actualPage.value = <string>localStorage.getItem("actualPage")
  const changeSearchPage = (increment:number) => {
    searchPage.value += increment
    localStorage.setItem("searchPage", `${searchPage.value}`)
    localStorage.setItem("actualPage", actualPage.value)
  }
  const changeSearchPageUrl = (url:string) => {
    actualPage.value = url
  } 
  return {
    searchPage,
    changeSearchPage,
    actualPage,
    changeSearchPageUrl
  }
})