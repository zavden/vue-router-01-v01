import { defineStore } from "pinia";
import { ref } from "vue"

export const useFavorito = defineStore('favorito', () => {
  const favorito = ref<any[]>([])

  if(localStorage.getItem("favoritos"))
    favorito.value = JSON.parse(<string>localStorage.getItem("favoritos"))

  const changeFavorito = (newFavorito:string) => {
    favorito.value.push(newFavorito)
    localStorage.setItem("favoritos", JSON.stringify(favorito.value))
  }
  const getIndexObj = (name:string):number => {
    let index = -1
    favorito.value.forEach((e, i:number) => {
      if(e.name === name){
          index = i
      }
    })
    return index
  }
  const removeElement = (name:string) => {
    const index = getIndexObj(name)
    console.log(index)
    if(index > -1)
      favorito.value.splice(index, 1)
    else
      console.warn("Error not founded index")
    localStorage.setItem("favoritos", JSON.stringify(favorito.value))
  }
  return {
    favorito,
    changeFavorito,
    getIndexObj,
    removeElement
  }
})