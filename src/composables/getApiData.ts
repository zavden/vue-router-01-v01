import axios from "axios"
import { ref } from "vue"

export const useGetApiData = () => {
  const data = ref<any>(null)
  const isLoading = ref<boolean>(true)
  const err404 = ref<boolean>(false)

  const getApiData = async (url:string) => {
    isLoading.value = true
    try {
      const res = await axios.get(url)
      data.value = res.data
    } catch (error) {
      console.log(error)
      err404.value = true
    } finally {
      isLoading.value = false
    }
  }

  return {
    getApiData,
    data,
    isLoading,
    err404
  }
}